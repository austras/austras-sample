package dev.austras.sample;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Main {
    private final TheService service;

    @Inject
    public Main(TheService service) {
        this.service = service;
    }

    public static void main(String[] args) {
        System.out.println("Main executed");
        var m = new Main(new TheService());
        m.startup(null);

        if (args.length == 0) {
            return;
        }
        System.out.println("Bye " + args[0]);
    }

    public void startup(@Observes @Initialized(ApplicationScoped.class) Object event) {
        service.say();
    }
}
