package dev.austras.sample;

import javax.inject.Singleton;

@Singleton
public class TheService {
    public void say() {
        System.out.println("Hello!");
    }
}
